package generic;

/**
 * @author ：徐梦轲
 * @date ：2022/5/17 0017 12:29
 */

public class ArrayAlg {
    public static Pair<String> minmax(String[] arr) {
        if (arr == null || arr.length == 0) {
            return null;
        }
        String min = arr[0];
        String max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (min.compareTo(arr[i]) >  0) {
                min = arr[i];
            }
            if (max.compareTo(arr[i]) < 0 ) {
                max = arr[i];
            }
        }
        return new Pair<>(min,max);
    }
    /**
     * 定义一个泛型方法，将类型变量置于修饰符之后，方法名之前
     */

    public static <T> T getMiddle(T[] arr){
        return arr[arr.length/2];
    }
}
