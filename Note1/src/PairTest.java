import generic.ArrayAlg;
import generic.Pair;

/**
 * 定义简单泛型类
 * @author ：徐梦轲
 * @date ：2022/5/17 0017 12:27
 */

public class PairTest {
    public static void main(String[] args) {
        String[] word = {"mary","had","a","little","lamb"};
        Pair<String> pair = ArrayAlg.minmax(word);
        System.out.println("min = " + pair.getFirst());
        System.out.println("max = " + pair.getSecond());
        //调用泛型方法的时候将具体类型包围在尖括号中，置于方法名前（大部分情况可以省略方法名前的<类型参数>）
        String middle = ArrayAlg.<String>getMiddle(word);
        System.out.println(middle);
    }
}
