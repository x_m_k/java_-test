import generic.ArrayAlg;
import generic.Pair;

import java.time.LocalDate;

/**
 * 类或方法对泛型类型变量的限定（上界）
 * @author ：徐梦轲
 * @date ：2022/5/17 0017 13:07
 */

public class PairTest {
    public static void main(String[] args) {
        LocalDate[] birthday = {
                LocalDate.of(1999,2,3),
                LocalDate.of(1939,6,13),
                LocalDate.of(1959,4,23),
                LocalDate.of(1979,8,12)
        };
        Pair<LocalDate> time = ArrayAlg.minmax(birthday);
        System.out.println("min = " + time.getFirst());
        System.out.println("max = " + time.getSecond());
    }
}
