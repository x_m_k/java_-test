package generic;

/**
 * @author ：徐梦轲
 * @date ：2022/5/17 0017 13:08
 */

public class ArrayAlg {
    //类型参数T意味着它可以是任何一个类，但该类的对象需要调用compareTo方法，所以这个类中必须要有compareTo方法。
    //故必须对类型变量T加一个限定，T必须是实现了Comparable接口的类。
    //使用关键字extends进行限定，<T extends 限定类型>，T必须是限定类型或着限定类型的子类。
    //T 和 限定类型 可以是类，也可以是接口。
    public static <T extends Comparable> Pair<T> minmax(T[] arr) {
        if (arr == null || arr.length == 0) {
            return null;
        }
        T min = arr[0];
        T max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (min.compareTo(arr[i]) >  0) {
                min = arr[i];
            }
            if (max.compareTo(arr[i]) < 0 ) {
                max = arr[i];
            }
        }
        return new Pair<>(min,max);
    }
}
