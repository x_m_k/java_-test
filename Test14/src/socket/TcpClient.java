package socket;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 * @author 徐梦轲
 * @create 2022-06-25 17:18
 */
public class TcpClient {
    private Socket socket = null;
    private String serverIp;
    private int severPort;

    public TcpClient(String serverIp, int severPort) throws IOException {
        this.serverIp = serverIp;
        this.severPort = severPort;
        socket = new Socket(serverIp,severPort);
    }
    public void start() {
        Scanner sc = new Scanner(System.in);
        try (InputStream is = socket.getInputStream();
             OutputStream os = socket.getOutputStream()) {
                while (true) {
                    System.out.println("请输入");
                    String request = sc.next();
                    if (request.equals("exit")) break;
                    PrintWriter writer = new PrintWriter(os);
                    writer.println(request);
                    writer.flush();
                    Scanner scanner = new Scanner(is);
                    String response = scanner.next();
                    String log = String.format("request : %s response : %s",
                            request,response);
                    System.out.println(log);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) throws IOException {
        TcpClient client = new TcpClient("127.0.0.1",9090);
        client.start();
    }
}
