package linklist;

/**
 * @author ：徐梦轲
 * @date ：2022/5/22 0022 8:44
 */

public class MyLinkListIndexOutOfException extends RuntimeException {
    public MyLinkListIndexOutOfException() {
    }

    public MyLinkListIndexOutOfException(String message) {
        super(message);
    }
}
