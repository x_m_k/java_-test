package linklist;

/**
 * @author ：徐梦轲
 * @date ：2022/5/21 0021 21:34
 */

public class MyLinkList {
    static class ListNode {
        private int val;
        private ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }

    private ListNode head;

    public void creatList() {
        ListNode listNode1 = new ListNode(11);
        ListNode listNode2 = new ListNode(34);
        ListNode listNode3 = new ListNode(16);
        ListNode listNode4 = new ListNode(76);
        ListNode listNode5 = new ListNode(13);
        listNode1.next = listNode2;
        listNode2.next = listNode3;
        listNode3.next = listNode4;
        listNode4.next = listNode5;
        this.head = listNode1;
    }

    public void displayList() {
        ListNode cur = head;
        while(cur != null) {
            System.out.println(cur.val + " ");
            cur = cur.next;
        }
    }

    //查找链表中是否包含关键字key
    public boolean contains(int key) {
        ListNode cur = this.head;
        while(cur != null) {
            if (cur.val == key) {
                return true;
            }
            cur = cur.next;
        }
        return false;
    }

    public int size() {
        int count = 0;
        ListNode cur = this.head;
        while(cur != null) {
            count++;
            cur = cur.next;
        }
        return count;
    }

    //头插法
    public void addFirst(int data) {
        ListNode listNode = new ListNode(data);
        listNode.next = this.head;
        this.head = listNode;
    }

    //尾插法
    public void addLast(int data) {
        ListNode listNode = new ListNode(data);
        ListNode cur = this.head;
        while(cur.next != null) {
            cur = cur.next;
        }
        cur.next = listNode;
        //listNode.next = null;
    }

    //指定位置插入
    public void addIndex(int index,int data) {
        checkIndexAdd(index);
        if (index == 0) {
            addFirst(data);
        }
        if (index == size()) {
            addLast(data);
        }
        ListNode node = new ListNode(data);
        ListNode cur = findIndexSubOne(index);
        node.next = cur.next;
        cur.next = node;
    }

    private ListNode findIndexSubOne(int index) {
        ListNode cur = this.head;
        while(index - 1 != 0) {
            cur = cur.next;
            index--;
        }
        return cur;
    }
    private void checkIndexAdd(int index) {
        if (index < 0 || index > size()) {
            throw new MyLinkListIndexOutOfException("插入位置不合法");
        }
    }

    //删除第一次出现key值的结点
    public void remove(int key) {
        if (this.head == null) {
            return;
        }
        if (this.head.val == key) {
            this.head = this.head.next;
            return;
        }
        ListNode cur = this.head;
        while(cur.next != null) {
            if (cur.next.val == key) {
                cur.next = cur.next.next;
                return;
            }
            cur = cur.next;
        }
    }

    //删除所有值为key的结点
    public void removeAllKey(int data) {
        if (this.head == null) {
            return;
        }
        ListNode pre = this.head;
        ListNode cur = this.head.next;
        while(cur != null) {
            if (cur.val == data) {
                pre.next = cur.next;
                cur = cur.next;
            }else {
                pre = cur;
                cur = cur.next;
            }
            if (this.head.val == data) {
                this.head = this.head.next;
            }
        }
    }

    //清空链表所有节点
    public void clear() {
         //this.head == null; 简单粗暴
         ListNode cur = this.head;
         ListNode curNext = null;
         while(cur != null) {
             curNext = cur.next;
             cur.next = null;
             cur = curNext;
         }
         head = null;
    }
}
