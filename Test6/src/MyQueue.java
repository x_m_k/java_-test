/**
 * @author 徐梦轲
 * @data 2022/5/26 0026 - 15:54
 */

import java.util.PrimitiveIterator;

/**
 *
 * @author     ：徐梦轲
 * @date       ：2022/5/26 0026 15:54
 */

public class MyQueue {

    static class Node {
         private int val;
         private Node next;

        public Node(int val) {
            this.val = val;
        }
    }

    public Node head;
    public Node last;
    public int usedSize = 0;

    public void offer(int val) {
        Node node = new Node(val);
        if (head == null) {
            head = node;
            last = node;
        } else {
            last.next = node;
            last = node;
        }
        usedSize++;
    }

    public int poll() {
        if (isEmpty()) {
            throw new RuntimeException("队列为空");
        }
        int val = head.val;
        head = head.next;
        if (head == null) {
            last = null;
        }
        usedSize--;
        return  val;
    }
    public boolean isEmpty() {
        return usedSize == 0;
    }
    public int peek() {
        if (isEmpty()) {
            throw new RuntimeException("队列为空");
        }
        return head.val;
    }
    public int size() {
        return usedSize;
    }
}
