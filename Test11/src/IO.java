import jdk.internal.org.objectweb.asm.tree.TryCatchBlockNode;

import javax.tools.DiagnosticListener;
import java.beans.beancontext.BeanContext;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author 徐梦轲
 * @create 2022-06-23 9:54
 */
public class IO {
    public static void main(String[] args) throws IOException {
        //读文件 1
        /*try (InputStream is = new FileInputStream("hello.txt")) {
            while (true) {
                //每次读一个字节
                int b = is.read();
                //返回-1时代表全部读完
                if (b == -1) break;
                System.out.println(b);
            }
        }*/
        //读文件 2:
        /*try (InputStream is = new FileInputStream("hello.txt")) {
            byte[] bytes = new byte[1024];
            int len = 0;
            while (true) {
                len = is.read(bytes);
                if (len == -1) break;
                for (int i = 0; i < len; i++) {
                    System.out.printf("%c",bytes[i]);
                }
            }
        }*/
        //读字符类型
        /*try(InputStream is = new FileInputStream("hello.txt")) {
            try (Scanner sc = new Scanner(is,"UTF-8")) {
                while (sc.hasNext()) {
                    String s = sc.next();
                    System.out.print(s);
                }
            }
        }*/

        //写文件 1:
        /*try(OutputStream os = new FileOutputStream("hello.txt")) {
            os.write('c');
            os.write('a');
            os.write('j');
            os.write('b');
            os.flush();
        }*/

        //写文件 2:
        /*try (OutputStream os = new FileOutputStream("hello.txt")) {
            byte[] bytes = new byte[] {
                    'a','b','c'
            };
            os.write(bytes);
            os.flush();
        }*/

        //写文件 3:
       /* try (OutputStream os = new FileOutputStream("hello.txt")) {
            String s = "hello";
            byte[] bytes = s.getBytes();
            os.write(bytes);
            os.flush();
        }*/

        //写文件 4:
       /* try (OutputStream os  = new FileOutputStream("hello.txt")) {
            String s = "你好中国";
            byte[] bytes = s.getBytes("utf-8");
            os.write(bytes);
            os.flush();
        }*/

        //写文件 5:
        /*
        try (OutputStream os = new FileOutputStream("hello.txt")) {
            try (OutputStreamWriter osWriter = new OutputStreamWriter(os,"utf-8")) {
                try (PrintWriter printWriter = new PrintWriter(osWriter)) {
                    printWriter.println("你好");
                    printWriter.println("hello");
                    printWriter.flush();
                }
            }
        }
        */

        //扫描指定目录，并找到名称中包含指定字符的所有普通文件（不包含目录），并且后续询问用户是否要删除该文件
        /*
        System.out.println("请输入要扫描的根目录:");
        Scanner sc = new Scanner(System.in);
        String path = sc.nextLine();
        File file = new File(path);
        if (!file.isDirectory()) {
            System.out.println("输入的目录不存在或不是目录");
            return;
        }
        System.out.println("请输入要找出文件名种的字符:");
        String token = sc.next();
        List<File> list = new ArrayList<>();
        scanDir(file,token,list);
        System.out.println("共找到符合条件的文件" + list.size() + "个");
        for (File x:list) {
            System.out.println(x.getCanonicalPath());
            System.out.println("是否删除该文件?(Y/N)");
            String in = sc.next();
            if (in.equals("Y")) x.delete();
        }

    }

    private static void scanDir(File file, String token, List<File> list) {
        File[] files = file.listFiles();
        if (files == null || files.length == 0)  return;
        for (File x:files) {
            if (x.isDirectory()) scanDir(x,token,list);
            else {
                if (x.getName().contains(token)) list.add(x);
            }
        }
    }
    */

        //进行普通文件的复制
       /* Scanner sc = new Scanner(System.in);
        System.out.println("请输入复制文件的路径:");
        String srcPath = sc.next();
        File srcFile = new File(srcPath);
        if (!srcFile.exists()) {
            System.out.println("文件不存在");
            return;
        }
        if (!srcFile.isFile()) {
            System.out.println("文件不是普通文件");
            return;
        }
        System.out.println("请输入复制的目标路径:");
        String destPath = sc.next();
        File destFile = new File(destPath);
        if (destFile.exists()) {
            System.out.println("目标路径已经存在");
            return;
        }
        try (InputStream is = new FileInputStream(srcFile)) {
            try (OutputStream os = new FileOutputStream(destFile)) {
                while (true) {
                    byte[] bytes = new byte[1024];
                    int len = is.read(bytes);
                    if (len == -1) break;
                    os.write(bytes,0,len);
                }
                os.flush();
            }
        }
        System.out.println("复制完成");*/



        //扫描指定目录，并找到名称或者内容中包含指定字符的所有普通文件（不包含目录）
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入目录路径:");
        String path = sc.next();
        File file = new File(path);
        if (!file.isDirectory()) {
            System.out.println("输入的不是目录或目录不存在");
            return;
        }
        System.out.println("请输入查找的指定字符:");
        String token = sc.next();
        List<File> list = new ArrayList<>();
        scanDir(file,token,list);
        for (File x:list) {
            System.out.println(x.getCanonicalPath());
        }

    }

    private static void scanDir(File file, String token, List<File> list) throws IOException {
        File[] files = file.listFiles();
        if (files == null || files.length == 0) return;
        for (File x:files) {
            if (x.isDirectory()) {
                scanDir(x,token,list);
            } else {
                if (isContains(x,token)) list.add(x);
            }

        }
    }

    private static boolean isContains(File x, String token) throws IOException {
        if (x.getName().contains(token)) return true;
        StringBuilder sb = new StringBuilder();
        try (InputStream is = new FileInputStream(x)) {
            try (Scanner sc = new Scanner(is,"utf-8")) {
                while (sc.hasNextLine()) {
                    sb.append(sc.nextLine());
                    sb.append("\n");
                }
            }
        }
        return sb.indexOf(token) != -1;
    }
}
