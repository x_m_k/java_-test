package generic;

import java.io.Serializable;

/**
 * @author ：徐梦轲
 * @date ：2022/5/17 0017 16:55
 */

public class Interval<T extends Comparable & Serializable> implements Serializable{
    //原始类型使用第一个限定来替换类型变量
    private T lower;
    private T upper;
}

/**
 * 原始类型Tnterval如下：
 * public class Interval implements Serializable{
 *     private Comparable lower;
 *     private Comparable upper;
 * }
 */
