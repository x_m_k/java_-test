package generic;

/**
 * @author ：徐梦轲
 * @date ：2022/5/17 0017 16:46
 */

public class Pair<T> {
    //无论何时定义一个泛型，都会编译时自动提供一个相应的原始类型，这个原始类型的名字就是去掉类型参数后的泛型类型名
    //原始类型将会被擦除，替换为限定类型（若无限定类型则替换为Object）
    private T first;
    private T second;
}
/**
 * Pair<T>的原始类型如下：
 * public class Pair{
 *     private Object first;
 *     private Object second;
 * }
 */