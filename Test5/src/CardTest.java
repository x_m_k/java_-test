import card.Card;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author ：徐梦轲
 * @date ：2022/5/21 0021 19:56
 */

public class CardTest {
    private static final String[] suits = {"♥","♠","♣","♦"};
    public static void playCard(List<Card> list) {
        List<List<Card>> hands = new ArrayList<>();
        List<Card> hand1 = new ArrayList<>();
        List<Card> hand2 = new ArrayList<>();
        List<Card> hand3 = new ArrayList<>();
        hands.add(hand1);
        hands.add(hand2);
        hands.add(hand3);
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 3; j++) {
                hands.get(j).add(list.remove(0));
            }
        }
        System.out.println("第一个人的牌：" + hand1);
        System.out.println("第二个人的牌：" + hand2);
        System.out.println("第三个人的牌：" + hand3);
    }
    public static void shuffleCard(List<Card> list) {
        Random random  = new Random();
        for (int i = list.size() - 1; i > 0 ; i--) {
            int index = random.nextInt(i);
            Card temp = list.get(i);
            list.set(i,list.get(index));
            list.set(index,temp);
        }
        System.out.println(list);
    }
    public static void showCard(List<Card> list) {
        for (int i = 0; i < 4; i++) {
            for (int j = 1; j <= 13; j++) {
                Card card = new Card(suits[i],j);
                list.add(card);
            }
        }
        System.out.println(list);
    }
    public static void main(String[] args) {
        List<Card> list = new ArrayList<>();
        showCard(list);
        System.out.println("洗牌：");
        shuffleCard(list);
        System.out.println("发牌：");
        playCard(list);
    }
}
