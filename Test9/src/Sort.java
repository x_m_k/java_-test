/**
 * @author 徐梦轲
 * @data 2022/6/6 0006 - 18:40
 */

import java.util.Arrays;

/**
 *
 * @author     ：徐梦轲
 * @date       ：2022/6/6 0006 18:40
 */

public class Sort {
    //直接插入排序
    public static void insertSort(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            int tmp = arr[i];
            int j = i - 1;
            for (; j >= 0; j--) {
                if (arr[j] > tmp) {
                    arr[j + 1] = arr[j];
                } else {
                    break;
                }
            }
            arr[j + 1] = tmp;
        }
    }

    //希尔排序
    public static void shell(int[] arr, int gap) {
        for (int i = gap; i < arr.length; i++) {
            int tmp = arr[i];
            int j = i - gap;
            for (; j >= 0; j -= gap) {
                if (arr[j] > tmp) {
                    arr[j + gap] = arr[j];
                } else {
                    break;
                }
            }
            arr[j + gap] = tmp;
        }
    }

    public static void shellSort(int[] arr) {
        int gap = arr.length;
        while (gap > 1) {
            shell(arr, gap);
            gap /= 2;
        }
        shell(arr, 1);
    }
    //简单选择排序
    public static void selectSort(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            int minIndex = i;
            int j = i+1;
            for (; j < arr.length; j++) {
                if (arr[j] < arr[minIndex]) minIndex = j;
            }
            int tmp = arr[i];
            arr[i] = arr[minIndex];
            arr[minIndex] = tmp;
        }
    }
    //堆排序
    public static void heapSort(int[] arr) {
        creatHeap(arr);
        int end = arr.length - 1;
        while (end >= 0) {
            swap(arr,0,end);
            shiftDown(arr,0,end);
            end--;
        }
    }
    private static void swap(int[] array,int i,int j) {
        int tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
    }
    public static void creatHeap(int[] arr) {
        for (int i = (arr.length-1-1) / 2; i >= 0; i--) {
            shiftDown(arr,i, arr.length);
        }
    }
    private static void shiftDown(int[] array,int root,int len) {
        int parent = root;
        int child = 2 * parent + 1;
        while (child < len) {
            if(child+1 < len && array[child] < array[child+1]) {
                child++;
            }
            if(array[child] > array[parent]) {
                swap(array,child,parent);
                parent = child;
                child = 2*parent+1;
            }else {
                break;
            }
        }
    }
    //冒泡排序
    public static void bubbleSort(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
// 设定一个标记，若为true，则表示此次循环没有进行交换，也就是待排序列已经有序，排序已经完成。
            boolean flag = true;
            for (int j = 0; j < arr.length - i - 1 ; j++) {
                if (arr[j] > arr[j + 1]) {
                    int tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                    flag = false;
                }
            }
            if (flag) break;
        }
    }
    public static void bubbleSort2(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            //从后往前冒泡 能快一丢丢~
// 设定一个标记，若为true，则表示此次循环没有进行交换，也就是待排序列已经有序，排序已经完成。
            boolean flag = true;
            for (int j = arr.length - 2; j >= i; j--) {
                if (arr[j] > arr[j + 1]) {
                    int tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                    flag = false;
                }
            }
            if (flag) break;
        }
    }
    public static void main(String[] args) {
        int[] arr = {7, 5, 34, 25,12};
        shellSort(arr);
        System.out.println(Arrays.toString(arr));
    }

}


