package stack;

import java.util.Arrays;

/**
 * @author ：徐梦轲
 * @date ：2022/5/23 0023 21:54
 */

public class MyStack {
    private int[] elem;
    private int usedSize;

    public MyStack(int[] elem) {
        this.elem = new int[5];
    }

    public void push(int val) {
        if (isFull()) {
            this.elem = Arrays.copyOf(this.elem,this.elem.length * 2);
        }
        this.elem[usedSize] = val;
        usedSize++;
    }

    public boolean isFull() {
        return this.usedSize == elem.length;
    }

    public int pop() {
        if (isEmpty()) {
            throw new RuntimeException("栈为空");
        }
        int ret = this.elem[usedSize - 1];
        usedSize--;
        return ret;
    }

    public boolean isEmpty() {
        return usedSize == 0;
    }

    public int peek() {
        if (isEmpty()) {
            throw new RuntimeException("栈为空");
        }
        return this.elem[usedSize - 1];
    }
    public int getUsedSize() {
        return usedSize;
    }
}
